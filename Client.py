import socket
import io
from PIL import Image

image = Image.open("test.png")
image = image.crop((50, 50, 200, 200))

imageByteArr = io.BytesIO()
image.save(imageByteArr, format='PNG')
imageByteArr = imageByteArr.getvalue()
imageSizeBytes = len(imageByteArr).to_bytes(4, "big")

clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print("Connecting...")
clientSocket.connect(("localhost", 49153))

print("Sending...")
clientSocket.sendall(imageSizeBytes)
clientSocket.sendall(imageByteArr)
print("Sent!")

clientSocket.close()
