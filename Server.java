import java.io.IOException;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;

import javax.imageio.ImageIO;

import java.awt.image.BufferedImage;

public class Server {
    public static void main(String[] args) {
        System.out.println("Awaiting connection...");
        try (
            ServerSocket serverSocket = new ServerSocket(49153);
            Socket clientSocket = serverSocket.accept();
        ) {
            System.out.println("Connected!\nAttempting to read image...");
            InputStream inStream = clientSocket.getInputStream();

            byte[] imageSizeBytes = inStream.readNBytes(4);
            int imageSize = ByteBuffer.wrap(imageSizeBytes).getInt();

            byte[] imageBytes = inStream.readNBytes(imageSize);
            BufferedImage image = ImageIO.read(new ByteArrayInputStream(imageBytes));

            System.out.println("Saving read image...");
            File outputFile = new File("receivedImage.png");
            ImageIO.write(image, "png", outputFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
